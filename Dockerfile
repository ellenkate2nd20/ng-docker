FROM node:16-alpine
WORKDIR /ng-docker

# RUN apk update && apk upgrade && apk add --no-cache bash git openssh

COPY ./package.json ./yarn.lock ./

RUN yarn install

COPY . /ng-docker

# CMD ["ng", "serve", "--host", "0.0.0.0", "--poll"]