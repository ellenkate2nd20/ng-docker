require('fp-ts/lib/function').pipe(
  require('dotenv').config(),
  () => require('child_process').exec(`ng serve --port=${process.env.PORT} --host 0.0.0.0 --poll`),
  child => child.stdout.on('data', err => console.log(err.toString()))
)